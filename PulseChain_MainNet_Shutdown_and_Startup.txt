*INSTRUCTIONS: Use lines 3 and 5 to to stop your validator before powering down the NUC. Use lines 9 thru 15 to start up again. Use 19 thru 23 to watch the logs in real time. On line 11 and 13 change the ETH/PLS address to yours (otherwise it burns)*

sudo docker stop -t 180 validator && sudo docker stop -t 180 beacon && sudo docker stop -t 180 geth

sudo docker container prune -f && sudo docker image prune --all --force

*INSTRUCTIONS:Use lines 9 thru 15 to start up again.

sudo docker run -d --name geth --network=host -v /home/admxn/blockchain:/blockchain registry.gitlab.com/pulsechaincom/go-pulse:latest --pulsechain --authrpc.jwtsecret=/blockchain/jwt.hex --datadir=/blockchain --maxpeers 150 --db.engine=leveldb

sudo docker run -d --name beacon --network=host -v /home/admxn/blockchain:/blockchain registry.gitlab.com/pulsechaincom/prysm-pulse/beacon-chain:latest --pulsechain --jwt-secret=/blockchain/jwt.hex --datadir=/blockchain --checkpoint-sync-url=https://checkpoint.pulsechain.com --genesis-beacon-api-url=https://checkpoint.pulsechain.com --subscribe-all-subnets --p2p-max-peers 150 --min-sync-peers=1 --suggested-fee-recipient=0x995f30748cD6F9bD2120864487BF799079e7BfBe

sudo docker run -d --name validator --network=host -v /home/admxn/blockchain/validator_keys:/keys -v /home/admxn/blockchain/pw:/wallet registry.gitlab.com/pulsechaincom/prysm-pulse/validator:latest --pulsechain --wallet-dir=/wallet --wallet-password-file=/wallet/pw.txt --graffiti validatorstore.com --suggested-fee-recipient=0x995f30748cD6F9bD2120864487BF799079e7BfBe

sudo docker update --restart always geth && sudo docker update --restart always beacon && sudo docker update --restart always validator

*INSTRUCTIONS: Do the following three lines 27, 29, and 31 in seperate terminal windows each so you can watch what is happening for all three containers (geth, beacon, validator)*

sudo docker logs --follow geth

sudo docker logs --follow beacon

sudo docker logs --follow validator
