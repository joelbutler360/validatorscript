*INSTRUCTIONS: Use this to withdraw all your PLS and exit the network, takes 24 hours to go into effect, keep validating until it shows exited on beacon.pulsechain.com for your validator*

sudo docker run -it --net=host --name=validatorexit -v /home/admxn/blockchain/pw:/wallet registry.gitlab.com/pulsechaincom/prysm-pulse/prysmctl:latest validator exit --wallet-dir=/wallet --beacon-rpc-provider=127.0.0.1:4000

*INSTRUCTIONS: Line 7 is the password it asks for at the end*

Exit my validator

